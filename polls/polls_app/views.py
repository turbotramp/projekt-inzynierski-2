from django.shortcuts import render
from .models import Question


def index(request):
    return render(request, 'templates/index.html')


def display_user_questions(request):
    try:
        user_questions = Question.objects.filter(for_user=request.user)
        context = {'user_questions': user_questions}
    except:
        return render(request, 'home.html')
    return render(request, 'home.html', context)