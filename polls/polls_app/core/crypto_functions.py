from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend


def gen_private_key():
    key = rsa.generate_private_key(backend=default_backend(), public_exponent=65537, \
    key_size=2048)
    print(key)
    return key

def get_private_key(key):
    em = key.private_bytes(encoding=serialization.Encoding.PEM,
    format=serialization.PrivateFormat.TraditionalOpenSSL,
    encryption_algorithm=serialization.NoEncryption())
    print(em)
    return em


pkey = gen_private_key()
get_private_key(pkey)