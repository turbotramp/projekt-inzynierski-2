from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

import datetime


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    is_teacher = models.BooleanField('teacher status', default=False)
    is_student = models.BooleanField('student status', default=False)

    def __str__(self):
        return self.user.username

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    for_user = models.ManyToManyField(User)
    #valid_until = models.DateTimeField('valid until')

    def __str__(self):
        return self.question_text

    def published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class Answer(models.Model):
    #question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_text = models.CharField(max_length=500)
    token_encrypted = models.TextField(max_length=256)


class Question_choices(models.Model):
    question_text = models.CharField(max_length=200)
    

class Choice(models.Model):
    #question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


