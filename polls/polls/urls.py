
from django.contrib import admin
from django.urls import path, include
from polls_app import views
from django.views.generic.base import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    #path('', index, name='index'),
    #path('', TemplateView.as_view(template_name='home.html'), name='home'), 
    path('', views.display_user_questions, name='display_user_questions'),
]


